//
// main.cpp for MY_SORT in /home/josso/en_cours/exo_cobra
// 
// Made by Arthur Josso
// Login   <arthur.josso@epitech.eu>
// 
// Started on  Thu Jun  2 14:24:08 2016 Arthur Josso
// Last update Thu Jun  2 14:24:09 2016 Arthur Josso
//

#include <iostream>
#include <fstream>
#include <cstdlib>

static int     	*get_values_in_tab(int &tab_size)
{
  std::ifstream	file;
  int		*tab;
  char		buff[16];

  tab_size = 0;
  tab = NULL;
  file.open("list");
  while (file.getline(buff, 16))
    {
      tab_size++;
      tab = (int*)realloc(tab, sizeof(int) * tab_size);
      tab[tab_size - 1] = atoi(buff);
    }
  file.close();
  return (tab);
}

int	main()
{
  int	*tab;
  int	tab_size;

  // Recupere les valeurs du fichier dans un tableau de nombre "tab" de taille "tab_size"
  if ((tab = get_values_in_tab(tab_size)) == NULL)
    return (1);
  // Montre le contenu du fichier (utile pour la debug)
  for (int i = 0; i < tab_size; i++)
    std:: cout << tab[i] << std::endl;
  return (0);
}
